package com.seejoke.dataway.configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;
import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;
import org.springframework.stereotype.Component;

/**
 * @author yangzhongying
 * @date 2020/4/14 11:55
 * @see com.seejoke.dataway.configuration.DataWayModule
 */
@DimModule
@Component
public class DataWayModule implements SpringModule {

  @Resource private DataSource dataSource = null;

  @Override
  public void loadModule(ApiBinder apiBinder) throws Throwable {
    apiBinder.installModule(new JdbcModule(Level.Full, this.dataSource));
  }
}
